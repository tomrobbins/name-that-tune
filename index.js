const http = require("http")
const path = require("path");
const process = require('process');

const express = require("express");
const fetch = require("node-fetch");
const socketIO = require("socket.io")

// setup express server
const app = express();
const server = http.createServer(app);
const io = socketIO(server);
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || "dev"

server.listen(port, () => {
    console.log("Server listening at port %d", port);
});

console.log(path.join(__dirname, "public"))
// app.get("/room/:id", () => {
//   app.use(express.static(path.join(__dirname, "public")))
// })

app.use("/", express.static(path.join(__dirname, "public")))
app.use("/room/:id", express.static(path.join(__dirname, "public/room")))

const GUESS_TIMEOUT_MS = 3000;
let GLOBAL_REGISTRY = {}

io.on("connection", (socket) => {

    const filterResults = (response) => {
        let results = Array();
        response.results.forEach((result) => {
            if (result.kind === "song") {
                results.push({
                    artistName: result.artistName,
                    trackName: result.trackName,
                    previewUrl: result.previewUrl,
                    trackViewUrl: result.trackViewUrl,
                    artworkUrl60: result.artworkUrl60,
                    artworkUrl100: result.artworkUrl100,
                });
            }
        });
        return results.slice(0, 5);
    };

    const queryItunesAPI = (query, socket) => {
        let start = Date.now();
        let url = encodeURI("https://itunes.apple.com/search?media=music&limit=10&term=" + query);
        log(url);
        fetch(url)
            .then((res) => res.json())
            .then((data) => {
                let filtered = filterResults(data);
                log(filtered);
                socket.emit("set search results", {
                    results: filtered,
                    timestamp: start,
                });
            });
    };

    const addUser = (roomID, socketID, username) => {
        const user = {
            username: username,
            score: 0,
            isMaster: false,
            lastGuessTimestamp: null,
        }
        if (GLOBAL_REGISTRY.hasOwnProperty(roomID)) {
            if (!GLOBAL_REGISTRY[roomID].hasOwnProperty(socketID)) {
                GLOBAL_REGISTRY[roomID][socketID] = user
                log(`added user: ${user.username}`)
            }
        } else {
            log(`failed to add user: ${user.username}`)
        }
    }

    const removeUser = (roomID, socketID, username) => {
        if (GLOBAL_REGISTRY.hasOwnProperty(roomID)) {
            if (GLOBAL_REGISTRY[roomID].hasOwnProperty(socketID)) {
                const user = GLOBAL_REGISTRY[roomID][socketID]
                delete GLOBAL_REGISTRY[roomID][socketID]
                log(`removed User: ${user.username}`)
            }
        } else {
            log(`failed to remove user: ${{username: username, socketID: socketID}}`)
        }
    }

    const getUUID = () => {
        const rand = Math.floor(Math.random() * 100000)
        const timestamp = Date.now()
        return parseInt(rand.toString() + timestamp.toString())
    }

    const log = function (msg) {
        if (env !== "production") {
            console.log(roomID, ...arguments)
        }
    };

    const roomID = socket.handshake.query["room"]

    if (!GLOBAL_REGISTRY.hasOwnProperty(roomID)) {
        GLOBAL_REGISTRY[roomID] = {}
    }

    log("Socket connected: " + socket.id)
    socket.join(roomID)

    // when the client emits 'new message', this listens and executes
    socket.on("new message", (data) => {
        // we tell the client to execute 'new message'
        socket.to(roomID).emit("new message", {
            socketID: socket.id,
            message: data.message,
        });
    });

    // when the client emits 'add user', this listens and executes
    socket.on("add user", (data) => {
        if (GLOBAL_REGISTRY[roomID].hasOwnProperty(socket.id)) {
            return;
        }

        addUser(roomID, socket.id, data.username)
        socket.emit("login", {
            numUsers: Object.keys(GLOBAL_REGISTRY[roomID]).length,
        });
        // echo globally (all clients) that a person has connected
        socket.to(roomID).emit("user joined", {
            socketID: socket.id,
            numUsers: Object.keys(GLOBAL_REGISTRY[roomID]).length,
        });

        io.in(roomID).emit("update users", GLOBAL_REGISTRY[roomID]);
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on("typing", (data) => {
        socket.to(roomID).emit("typing", {
            socketID: socket.id,
        });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on("stop typing", (data) => {
        socket.to(roomID).emit("stop typing", {
            socketID: socket.id,
        });
    });

    // when the user disconnects.. perform this
    socket.on("disconnect", (data) => {
        if (GLOBAL_REGISTRY[roomID].hasOwnProperty(socket.id)) {
            removeUser(roomID, socket.id, socket.username)

            // echo globally that this client has left
            socket.to(roomID).emit("user left", {
                socketID: socket.id,
                numUsers: Object.keys(GLOBAL_REGISTRY[roomID]).length,
            });

            io.in(roomID).emit("update users", GLOBAL_REGISTRY[roomID]);
        }
    });

    // when the client emits 'guess', we broadcast it to others
    socket.on("guess", () => {
        let timestamp = Date.now();
        const lastGuess = GLOBAL_REGISTRY[roomID][socket.id].lastGuessTimestamp
        if (lastGuess === null || timestamp - lastGuess > GUESS_TIMEOUT_MS) {
            GLOBAL_REGISTRY[roomID][socket.id].lastGuessTimestamp = timestamp;
            io.in(roomID).emit("guess", {
                socketID: socket.id,
            });
        }
    });

    socket.on("set master", (data) => {
        log(socket.id + " is now the master");
        // remove master from everyone else
        Object.keys(GLOBAL_REGISTRY[roomID]).forEach((socketID) => {
            GLOBAL_REGISTRY[roomID][socketID].isMaster = false
        })
        // set master on this socket
        GLOBAL_REGISTRY[roomID][socket.id].isMaster = true
        io.in(roomID).emit("set master", {
            socketID: socket.id,
        });
    });

    socket.on("give up master", (data) => {
        if (GLOBAL_REGISTRY[roomID][socket.id].isMaster) {
            log(socket.id + " is no longer the master");
            io.in(roomID).emit("give up master", {
                socketID: socket.id,
            });
        }
    });

    socket.on("query itunes", (data) => {
        if (GLOBAL_REGISTRY[roomID][socket.id].isMaster) {
            log(GLOBAL_REGISTRY[roomID][socket.id].username + " is querying itunes for " + data.query);
            queryItunesAPI(data.query, socket);
        } else {
            log(`${GLOBAL_REGISTRY[roomID][socket.id].username} tried to query itunes but they are not the master.`);
        }
    });

    socket.on("set song", (data) => {
        if (GLOBAL_REGISTRY[roomID][socket.id].isMaster) {
            log(socket.id + " is setting the song to " + JSON.stringify(data.song));
            socket.to(roomID).emit("set song", data.song);
        } else {
            log(`${socket.id} tried to set the song but they are not the master.`);
        }
    });

    socket.on("play song", (data) => {
        io.in(roomID).emit("play song", data.clipLength);
    });

    socket.on("update scoreboard", (data) => {
        if (GLOBAL_REGISTRY[roomID].hasOwnProperty(data.socketID)) {
            GLOBAL_REGISTRY[roomID][data.socketID].score = data.score
            io.in(roomID).emit("update users", GLOBAL_REGISTRY[roomID]);
        }
    });

    // Room stuff
    socket.on("request room", () => {
        const roomID = getUUID()
        log("Assigning room " + roomID + " to socket " + socket.id)
        socket.emit("assign room", roomID)
    })

    socket.on("get statistics", () => {
        let rooms = 0, players = 0
        Object.keys(GLOBAL_REGISTRY).forEach((r) => {
            const numPlayers = Object.keys(GLOBAL_REGISTRY[r]).length
            if (numPlayers > 0) {
                rooms++
                players += numPlayers
            }
        });
        socket.emit("set statistics", {numPlayers: players, numRooms: rooms})
    })
});
