const socket = io.connect();

const inviteContainer = document.getElementsByClassName("inviteContainer")[0];
const generateButton = document.getElementById("generateButton");

const inviteLink = document.getElementById("inviteLink");

const copyButton = document.getElementById("copyButton");

const statistics = document.getElementsByClassName("statistics")[0];
const numPlayers = document.getElementById("numPlayers");
const numRooms = document.getElementById("numRooms");


const updateInviteLink = (roomID) => {
    const link = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/room/${roomID}`
    inviteLink.textContent = link
    inviteLink.href = link
    inviteContainer.classList.remove("hidden")

}

const getStatistics = () => {
    console.log("getting")
    socket.emit("get statistics");
}

const setStatistics = (data) => {
    console.log("setting", data)
    numPlayers.textContent = data.numPlayers;
    numRooms.textContent = data.numRooms;
    statistics.classList.remove("fade")
}

// Window Events
window.addEventListener("load", getStatistics)

// Timeout Events
window.setInterval(getStatistics, 5000)

// Button Events

generateButton.addEventListener("click", () => {
    socket.emit("request room")
})

copyButton.addEventListener("click", () => {
    let el = document.createElement("input")
    el.display = "none"
    el.value = inviteLink.href
    document.body.appendChild(el)
    el.select()

    document.execCommand("copy")
    document.body.removeChild(el)
})

// Socket Events

socket.on("assign room", (roomID) => {
    updateInviteLink(roomID)
})

socket.on("set statistics", (data) => {
    setStatistics(data)
})