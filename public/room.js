var TYPING_TIMER_LENGTH = 400; // ms
var COLORS = [
    "#e21400",
    "#91580f",
    "#f8a700",
    "#f78b00",
    "#58dc00",
    "#287b00",
    "#a8f07a",
    "#4ae8c4",
    "#3b88eb",
    "#3824aa",
    "#a700ff",
    "#d300e7",
];
const USER_DELIMITER = "@@@"

// App Structure
const loginPage = document.querySelectorAll(".login.page")[0];
const gamePage = document.querySelectorAll(".game.page")[0];

const playerView = document.querySelectorAll(".gameView.player")[0];
const masterView = document.querySelectorAll(".gameView.master")[0];


// Login Page
const usernameInput = document.querySelectorAll(".usernameInput")[0];
const usernameForm = document.querySelectorAll(".usernameForm")[0];

// Game Page

// Chat Section
const messages = document.querySelectorAll(".messages")[0];
const messageInput = document.querySelectorAll(".messageInput")[0];
const messageForm = document.querySelectorAll(".messageForm")[0];

// Player View
const guessButton = document.querySelectorAll(".guessButton")[0];
const masterButton = document.querySelectorAll(".masterButton")[0];

// Master View
const songSearchBar = document.querySelectorAll(".songSearchBar")[0];
let searchResults = document.querySelectorAll(".searchResults")[0]
let playlist = document.querySelectorAll(".playlist")[0];
const giveUpMasterButton = document.querySelectorAll(".giveUpMasterButton")[0];

// Audio Player
let audioPlayer = document.querySelectorAll(".audioPlayer")[0];
const speaker = document.querySelectorAll(".speaker")[0];

// Scoreboard
let scoreboard = document.querySelectorAll(".scoreboard")[0];
let REGISTRY = {};


// Initialize state
let username;
let connected = false;
let typing = false;
let lastTypingTimestamp;
let lastSearchResultsTimestamp = null;

let currentInput;
let currentPage;
let currentView;
let currentSong;


const room = window.location.pathname

const socket = io.connect("", {"query": "room=" + room});

window.addEventListener("load", () => {
    switchPage(loginPage)
    switchView(playerView)
    switchInput(usernameInput)
})

const addParticipantsMessage = (data) => {
    let message = "";
    if (data.numUsers === 1) {
        message += "there's 1 participant";
    } else {
        message += "there are " + data.numUsers + " participants";
    }
    log(message);
};

// Sets the client's username
const setUsername = () => {
    username = cleanInput(usernameInput.value.trim());
    // If the username is valid
    if (username) {
        switchPage(gamePage)
        switchInput(messageInput)
        socket.emit("add user", {username: username});
    }
};

const switchPage = (page) => {
    if (page !== currentPage) {
        page.classList.add("shown")
        if (currentPage) {
            currentPage.classList.remove("shown")
        }
        currentPage = page;
    }
}

const switchView = (view) => {
    if (view !== currentView) {
        view.classList.add("shown")
        if (currentView) {
            currentView.classList.remove("shown")
        }
        currentView = view;
    }

    // automatically focus the default input for this view
    if (view === masterView) {
        switchInput(songSearchBar)
    } else if (view === playerView) {
        switchInput(messageInput)
    }
};

// Sets the client to master mode
const onSetMaster = (socketID) => {
    log(`${REGISTRY[socketID].username} is now the master`);
    if (socketID === socket.id) {
        switchView(masterView);
        setPlayButtonsDisabled(true);
    } else {
        switchView(playerView);
    }
};

const onGiveUpMaster = (socketID) => {
    log(`${REGISTRY[socketID].username} is no longer the master`);
    if (socketID === socket.id) {
        switchView(playerView);
    }
};

// Sends a chat message
const sendMessage = () => {
    let message = messageInput.value;
    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
        messageInput.value = "";
        addChatMessage({
            socketID: socket.id,
            message: message,
        });
        // tell server to execute 'new message' and send along one parameter
        socket.emit("new message", {message: message});
    }
};

// Log a message
const log = (message, options) => {
    const li = document.createElement("li")
    li.textContent = message
    li.className = "log"
    addMessageElement(li, options);
};

// Adds the visual chat message to the message list
const addChatMessage = (data, options) => {
    options = options || {};
    removeChatTyping(data)

    const usernameDiv = document.createElement("span")
    usernameDiv.classList.add("username")
    usernameDiv.textContent = REGISTRY[data.socketID].username
    usernameDiv.style.color = generateColor(data.socketID)

    const messageBodyDiv = document.createElement("span")
    messageBodyDiv.classList.add("messageBody");
    messageBodyDiv.textContent = data.message;

    const messageDiv = document.createElement("li")
    messageDiv.classList.add("message")
    if (data.typing) {
        messageDiv.classList.add("typing")
    }
    messageDiv.dataset.socketID = data.socketID
    messageDiv.appendChild(usernameDiv)
    messageDiv.appendChild(messageBodyDiv)

    addMessageElement(messageDiv, options);
};

// Adds the visual chat typing message
const addChatTyping = (data) => {
    data.typing = true;
    data.message = "is typing";
    addChatMessage(data);
};

// Removes the visual chat typing message
const removeChatTyping = (data) => {
    getTypingMessages(data).forEach((el) => {
        el.remove()
    })
};

// Adds a message element to the messages and scrolls to the bottom
// el - The element to add as a message
// options.prepend - If the element should prepend
//   all other messages (default = false)
const addMessageElement = (el, options) => {

    // Setup default options
    if (!options) {
        options = {};
    }
    if (typeof options.prepend === "undefined") {
        options.prepend = false;
    }
    if (typeof options.className != "undefined") {
        el.classList.add(options.className);
    }

    // Apply options
    if (options.prepend) {
        messages.insertBefore(el, messages.firstChild);
    } else {
        messages.appendChild(el);
    }

    // Scroll to show the bottom of the chat (most recent)
    messages.scrollTop = messages.scrollHeight;
};

// Prevents input from having injected markup
const cleanInput = (input) => {
    let div = document.createElement("div")
    div.textContent = input
    return div.innerHTML
};

// Updates the typing event
const updateTyping = () => {
    if (connected) {
        if (!typing) {
            typing = true;
            socket.emit("typing", {});
        }
        lastTypingTimestamp = new Date().getTime();

        setTimeout(() => {
            var typingTimer = new Date().getTime();
            var timeDiff = typingTimer - lastTypingTimestamp;
            if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                socket.emit("stop typing", {})
                typing = false;
            }
        }, TYPING_TIMER_LENGTH);
    }
};


// Make a guess
const makeGuess = () => {
    if (connected) {
        socket.emit("guess", {});
    }
};

// Gets the 'X is typing' messages of a user
const getTypingMessages = (data) => {
    const typingMessages = document.querySelectorAll(".typing.message") || []
    return Array.from(typingMessages).filter(function (el) {
        return el.dataset.socketID === data.socketID;
    });
};

// Gets the color for a string
const generateColor = (s) => {
    // Compute hash code
    let hash = 7;
    for (let i = 0; i < s.length; i++) {
        hash = s.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    let index = Math.abs(hash % COLORS.length);
    return COLORS[index];
};

// MESSAGING EVENTS

messageInput.addEventListener("input", () => {
    updateTyping();
});

messageForm.addEventListener("submit", (event) => {
    event.preventDefault();
    if (username) {
        sendMessage();
        socket.emit("stop typing", {});
        typing = false;
    }
});

const switchInput = (input) => {
    currentInput = input;
    currentInput.focus();
};

// Click events

// submit username
usernameForm.addEventListener("submit", (event) => {
    event.preventDefault();
    setUsername();
})

// Make a guess
guessButton.addEventListener("click", () => {
    makeGuess();
});

masterButton.addEventListener("click", () => {
    socket.emit("set master", {})
});

giveUpMasterButton.addEventListener("click", () => {
    socket.emit("give up master", {})
});

// Searching

let timeout = null;
songSearchBar.addEventListener("input", () => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
        socket.emit("query itunes", {
            query: cleanInput(songSearchBar.value)
        });
    }, 50);
});

const makeRow = (result, interactive) => {
    let row = document.createElement("tr")
    row.className = "searchResult";
    if (interactive) {
        row.classList.add("interactive")
    }
    row.previewUrl = result.previewUrl;

    // images
    let cell = row.insertCell();

    const artworkDiv = document.createElement("div")
    let artwork = document.createElement("img");
    artwork.src = result.artworkUrl60;
    artwork.className = "artwork"
    artworkDiv.appendChild(artwork)
    cell.appendChild(artworkDiv);

    // Add Apple Music badge for legal reasons
    const badgeDiv = document.createElement("div")
    let badge = document.createElement("img");
    badge.src = "Apple_Music_Small_Badge_RGB.svg"
    badge.alt = "Listen on Apple Music"
    badge.className = "badge"
    badgeDiv.appendChild(badge)
    cell.appendChild(badgeDiv);

    ["artistName", "trackName"].forEach((field) => {
        var cell = row.insertCell();
        cell.textContent = result[field];
        cell.className = field;
    });

    // make it clickable
    row.addEventListener("click", (e) => {
        // Follow Apple Music link for legal reasons
        if (e.target.className === "badge") {
            window.open(result.trackViewUrl)
        }
        if (interactive) {
            setPlaylist(result);
            setSong(result);
            socket.emit("set song", {song: result});
        }
    });

    return row
}

const setSearchResults = (data) => {
    if (lastSearchResultsTimestamp === null || data.timestamp > lastSearchResultsTimestamp) {
        lastSearchResultsTimestamp = data.timestamp;
        const results = data.results;
        let table = document.createElement("table");
        table.className = "searchResults";
        results.forEach((result) => {
            table.appendChild(makeRow(result, true))
        });
        searchResults.parentNode.replaceChild(table, searchResults)
        searchResults = table
    }
};

const setPlaylist = (result) => {
    let table = document.createElement("table");
    table.className = "playlist";
    table.insertRow();
    table.appendChild(makeRow(result, false))

    playlist.parentNode.replaceChild(table, playlist)
    playlist = table

    setPlayButtonsDisabled(false)
};

// Audio Controls

const setSong = (result) => {
    currentSong = result;
    var audio = document.createElement("audio");
    audio.className = "audioPlayer";
    audio.src = result.previewUrl;
    audio.preload = "auto";
    audioPlayer.parentNode.replaceChild(audio, audioPlayer)
    audioPlayer = audio;
};

const setPlayButtonsDisabled = (value) => {
    document.querySelectorAll(".playButton").forEach((el) => {
        el.disabled = value
    })

}

document.querySelectorAll(".playButton").forEach((el) => {
    el.addEventListener("click", () => {
        socket.emit("play song", {clipLength: el.dataset.cliplength});  // lowercase
    })
})

const stop = () => {
    speaker.classList.remove("playing");
    audioPlayer.pause();
    audioPlayer.currentTime = 0;
};

const playSong = (length) => {
    log("Starting the song (" + length + "s)...", {
        className: "songLog",
    });
    audioPlayer.play();
    speaker.classList.add("playing");
    setTimeout(stop, length * 1000);
};

const stopSong = (socketID) => {
    stop();
    log(`${REGISTRY[socketID].username} is making a guess!`, {className: "guessLog"});
};

// Users and Scoreboard

const updateUsers = (users) => {
    REGISTRY = users
    renderScoreboard()
};

const updateScoreboard = (socketID, score) => {
    socket.emit("update scoreboard", {socketID: socketID, score: score});
};

const renderScoreboard = () => {
    let table = document.createElement("table");
    table.className = "scoreboard";

    Object.keys(REGISTRY).forEach((socketID) => {
        const user = REGISTRY[socketID]
        table.insertRow();
        const row = table.rows[table.rows.length - 1];

        row.className = "scoreboardRow";

        const texts = [user.username, user.score, "-", "+"];
        const classes = [
            "scoreboardUsername",
            "scoreboardScore",
            "scoreboardMinus",
            "scoreboardPlus",
        ];
        const onClickFunctions = [
            () => {
            },
            () => {
            },
            () => {
                updateScoreboard(socketID, user.score - 1);
            },
            () => {
                updateScoreboard(socketID, user.score + 1);
            },
        ];
        for (let i = 0; i < texts.length; i++) {
            let cell = row.insertCell();
            cell.textContent = texts[i];
            cell.className = classes[i];
            cell.onclick = onClickFunctions[i];
        }
    });
    scoreboard.parentNode.replaceChild(table, scoreboard)
    scoreboard = table
};

// Socket events

socket.on("connect", () => {
    socket.emit("room", {room: room})
})

// Whenever the server emits 'login', log the login message
socket.on("login", (data) => {
    connected = true;
    const message = "Welcome to Name That Quaran-tune!";
    log(message, {
        prepend: true,
    });
    addParticipantsMessage(data);
});

// Whenever the server emits 'new message', update the chat body
socket.on("new message", (data) => {
    addChatMessage(data);
});

// Whenever the server emits 'user joined', log it in the chat body
socket.on("user joined", (data) => {
    log(`${REGISTRY[data.socketID].username} joined`);
    addParticipantsMessage(data);
});

// Whenever the server emits 'user left', log it in the chat body
socket.on("user left", (data) => {
    log(`${REGISTRY[data.socketID].username} left`);
    addParticipantsMessage(data);
    removeChatTyping(data);
});

socket.on("update users", (users) => {
    updateUsers(users);
});

// Whenever the server emits 'typing', show the typing message
socket.on("typing", (data) => {
    addChatTyping(data);
});

// Whenever the server emits 'stop typing', kill the typing message
socket.on("stop typing", (data) => {
    removeChatTyping(data);
});

socket.on("disconnect", () => {
    log("you have been disconnected");
});

socket.on("reconnect", () => {
    log("you have been reconnected");
    if (username) {
        socket.emit("add user", {username: username});
    }
});

socket.on("reconnect_error", () => {
    log("attempt to reconnect has failed");
});

socket.on("set master", (data) => {
    onSetMaster(data.socketID);
});

socket.on("give up master", (data) => {
    onGiveUpMaster(data.socketID);
});

socket.on("set search results", (data) => {
    setSearchResults(data);
});

socket.on("set song", (result) => {
    setSong(result);
});

socket.on("guess", (data) => {
    stopSong(data.socketID);
});

socket.on("play song", (clipLength) => {
    playSong(clipLength);
});
